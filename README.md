# Cisco VPN Vagrant
A small VM that has the Cisco Anyconnect VPN client installed.

# Usage
By default, the VPN connects using the username of your host computer. If your
username is different, just reset `$USER`.

```
$ vagrant up
$ ./connect.sh
```
