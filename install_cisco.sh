#!/bin/bash

set -e

VERSION=3.1.04066

cd /tmp
tar zxf /vagrant/clients/anyconnect-predeploy-linux-${VERSION}-k9.tar.gz
cd anyconnect-$VERSION/vpn
rm license.txt
./vpn_install.sh
cp /etc/ssl/certs/* /opt/.cisco/certificates/ca
