#!/bin/bash

set -e

echo "Starting VPN"
cd /opt/cisco/anyconnect/bin
echo -e "$1\n$2" | ./vpn connect cybfwchn002.cyberlab-na.com/GovFoundry

echo "Dropping evil firewall rules"
sudo iptables -F INPUT
sudo iptables -F OUTPUT
